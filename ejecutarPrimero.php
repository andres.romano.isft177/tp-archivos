<?php
//	incluimos con require, los archivos que tienen las funciones y donde declaramos la ubicacion de los archivos
require "funciones.php";
require "conexion.php";

// ahora con include incluimos los script .php que normalizan y crean los archivos
// SE EJECUTARA PRIMERO EL SCRIPT QUE NORMALIZA EL ARCHIVO
include "normalizarArchivo.php";
// SEGUDO EL SCRIPT QUE EXTRAE LAS CATEGORIAS Y CREA EL ARCHIVO CATEGORIAS
include "extraerCategoria.php";
// TERCERO EL SCRIPT QUE CREA EL NUEVO ARCHIVO DE ASENTAMIENTOS
include "crearArchivoFinal.php";

echo "Fin del proceso de normalizado y creado de nuevos archivos."."\n";

