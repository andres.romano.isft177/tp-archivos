<?php
require "funciones.php";
require "conexion.php";
require('./fpdf182/fpdf.php');

if (isset($_POST['reporte'])) {
	$idCategoria = $_POST['cat'];
	$pdf = new FPDF();
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',16);
	$pdf->Cell(40,10,'idAsentamiento|Categoria|Nombre Asentamiento|Provincia');
	$pdf->Ln(10);
	$pdf->SetFont('Arial','B',11);	
	$file = fopen($asentamientos, "r");
	while (!feof($file)) {
		$linea = fgets($file);
		$datos = explode("|", $linea);
		if ($idCategoria == $datos[1]) {
			$pdf->Cell(40,10,$datos[0]."|".nombreCategoria($categorias,$datos[1])."|".$datos[2]."|".utf8_decode($datos[3]));
			$pdf->Ln(10);
		}
	}
$pdf->Output();
}
