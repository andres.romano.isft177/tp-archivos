<?php

function buscar($compare,$datos) {// esta funcion va buscando datos ni bien va apareciendo en la recorrida del archivo, recibe un array y un valor
	$valor = count($compare);// mide el array auxiliar
	$found = 0;//contador para saber si aparece un dato nuevo
	for($i=0; $i<$valor; $i++) {	// va a iterar segun el tamaño del $compare el array auxiliar
		if ((strcmp($compare[$i],$datos) != 0) && ($datos != null)) {//si son distintos 
			$found++;			//incremento el contador
		}
	}
	if ($found == $valor) {// si la variables son iguales quiere decir que recorrio todo el array $compare y no encontro a $datos, y devuelve false
		return false;
	} else {// si son distintas quiere decir que encontro a $datos dentro de $compare y devuelve true
		return true;
	}
}
function idCategoria($archivo,$valor) {
	if (is_readable($archivo)) {
	$file = fopen($archivo, "r");// abro para leer
		while (!feof($file)) {
			$line = fgets($file);// obtengo la primer linea
			$datos = explode("|", $line);			
			if (strcmp(trim($datos[1]),trim($valor)) == 0) {		
				return $datos[0];
			} 			
		}
	fclose($file);
	}
}

function nombreCategoria($archivo,$valor) {
	if (is_readable($archivo)) {
	$file = fopen($archivo, "r");// abro para leer
		while (!feof($file)) {
			$line = fgets($file);// obtengo la primer linea
			$datos = explode("|", $line);			
			if (strcmp(trim($datos[0]),trim($valor)) == 0) {		
				return $datos[1];
			} 			
		}
	fclose($file);
	}
}
