<?php
require "conexion.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Consultar asentamientos</title>
	<meta charset="utf-8"/>
</head>
<body>
	<h1>Consultar asentamientos por Categoria</h1>
	<form action="reporte.php" method="post">
	<label for="cat">Categoria: </label>
	<select id="cat" name="cat">
		<?php
			if (is_readable($categorias)) {
    		$file = fopen($categorias,"r");
    			while (!feof($file)) {   
    				$linea = fgets($file);
    				if ($linea != null) {
    					$datos = explode("|",$linea);
    					echo "<option value='".$datos[0]."'>".$datos[1]."</option>";	
    				}
    			}
    		}
    	?>
	</select>
	<input type="submit" name="reporte" value="Reporte PDF">
	</form>
</body>
</html>