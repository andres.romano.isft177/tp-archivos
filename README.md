# Ejemplo de como trabajar con archivos de texto plano

Con funciones se extraen los datos que se repiten para normalizar
y crear nuevos archivos, también con un script de php se ejecutan
toda la normalización en simultáneo. Hay un ejemplo de web de 
consulta que también emite un reporte en PDF. Para que funcione
se debe descargar la librería [FPDF](http://www.fpdf.org/)

El ejemplo esta realizado con el archivo [asentamientos.csv](https://datos.gob.ar/dataset/jgm-servicio-normalizacion-datos-geograficos/archivo/jgm_8.13)

> Se debe contar con permisos sobre la carpeta del proyecto para que se generen los archivos